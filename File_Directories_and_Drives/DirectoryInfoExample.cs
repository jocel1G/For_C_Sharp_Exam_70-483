﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace File_Directories_and_Drives
{
    public static class DirectoryInfoExample
    {
        public static void HowToGrantAccess()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo("TestDirectory");
            directoryInfo.Create();
            DirectorySecurity directorySecurity = directoryInfo.GetAccessControl();
            SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            directorySecurity.AddAccessRule(
                new FileSystemAccessRule(everyone,
                    FileSystemRights.ReadAndExecute,
                    AccessControlType.Allow)
                );
            directoryInfo.SetAccessControl(directorySecurity);
        }
    }
}
