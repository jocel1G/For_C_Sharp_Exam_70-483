﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Await_and_Async
{
    public class AsyncAwait
    {
        #region Code de la page Day 2
        /*
        public static void Run()
        {
            string result = DownloadContent().Result;
            Console.WriteLine(result);
        }

        public static async Task<string> DownloadContent()
        {
            using (HttpClient client = new HttpClient())
            {
                string result = await client.GetStringAsync("http://www.microsoft.com");
                return result;
            }
        }
        */
        #endregion

        #region My code
        public async static void RunAsync()
        {
            Task<string> getStringTask = DownloadContentAsync();
            Console.WriteLine("Vous êtes dans Run");
            string result = await getStringTask;
            Console.WriteLine(result);
        }

        public static async Task<string> DownloadContentAsync()
        {
            using (HttpClient client = new HttpClient())
            {
                string result = await client.GetStringAsync("http://www.microsoft.com");
                Console.WriteLine("Vous êtes dans DownloadContentAsync");
                return result;
            }
        }
        #endregion
    }
}
