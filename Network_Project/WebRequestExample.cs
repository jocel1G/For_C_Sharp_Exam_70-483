﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Network_Project
{
    public class WebRequestExample
    {
        public static void ExampleOfWebRequest()
        {
            // Create a request for the URL.
            // Ou bien
            //WebRequest request = WebRequest.Create("http://www.google.com");
            // Ou bien
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.google.com");
            // Ou bien
            //WebResponse response = request.GetResponse();
            // Ou bien
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // Get the stream containing content returned by the server.
            using(Stream dataStream = response.GetResponseStream())
            {
                using (StreamReader responseStream = new StreamReader(dataStream))
                {
                    // Read the content.
                    string responseText = responseStream.ReadToEnd();
                    // Display the content.
                    Console.WriteLine(responseText);
                }
            }
        }
    }
}
