﻿using Arrays_Project;
using Async_IO_streams;
using Attributes_Project;
using Await_and_Async;
using Chapter1;
using Concurrent_Collections;
using Delegates;
using Delegates.lambda_expressions;
using Events_and_Callbacks;
using Exceptions_Project;
using File_Directories_and_Drives;
using Interface_Project;
using Logging_and_tracing;
using Multithreading_and_As_Pr;
using Multithreading_and_As_Pr.Tasks;
using Network_Project;
using ParallelClass;
using Precompiler_Directives;
using Queues_and_Stacks;
using Secure_string_data;
using Streams_Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Types_Project.Change_types;
using Types_Project.Generics;
using Types_Project.Reference_Types;
using Types_Project.Types_of_types;
using Using_PLINQ;
using Using_sets_Project;
using Working_with_encryption;

namespace returnsmart_70_483
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Manage program flow

            #region Day 1
            #region Multithreading and asynchronous processing
            #region Threads
            //Thread_Join.Run();
            //Threads1.Run();
            //Threads2.Run();
            //Threads3.Run();
            //ThreadStaticAttr.Run();
            //Threads4.Run();
            /*
            ManualResetEvent_Example manresev = new ManualResetEvent_Example();
            manresev.Run();
            */
            //ThreadPoolExample.Run();
            #endregion
            #endregion

            #region Tasks
            //Tasks1.Run();
            //Tasks2.Run();
            //Tasks3.Run();
            //Tasks4.Run();
            //Tasks5.Run();
            //Tasks6.Run();
            //Tasks7.Run();
            //CancToSour.Run();
            //Tasks8.Run();
            #endregion
            #endregion

            #region Day 2
            #region Parallel class
            //ParallelExample.Run();
            #endregion

            #region async and await
            //AsyncAwait.RunAsync();
            //AsyncAwait2.RunAsync();
            #endregion

            #region PLINQ
            //PLINQExample.Run();
            //PLINQExample2.Run();
            #endregion

            #region Concurrent collections
            //BlockingCollectionExample.Run();
            #endregion
            #endregion

            #region Day 3
            #region Delegates
            //Delegates_class.Run();
            //Covariance.Run();
            //Contravariance.Run();
            //Lambda4.Run();
            #endregion

            #region Events and callbacks
            //Events1.Run();
            //Events2.Run();
            /*
            Action<object, MyArgs> actForPub3 = (sender, e) =>
                {
                    Console.WriteLine("Event raised:{0}", e.Value);
                };
            Pub3 pub3 = new Pub3();
            //pub3.OnChange += (sender, e) => actForPub3(sender, e);
            pub3.OnChange += new EventHandler<MyArgs>(actForPub3);
            pub3.Run();
            */
            /*
            Events4 event4 = new Events4();
            event4.CreateAndRaise();
            */
            #endregion
            #endregion

            #region Day 4
            //Exceptions1.Run();
            //Exceptions2.Run();
            //Exceptions4.Run();
            #endregion

            #endregion

            #region Create and use types (24%)
            #region Day 5
            #region Types of types
            //Types2.Run();
            #endregion

            #region Generics
            #region Types5<T>
            // 12/10/2016 : Types5<T>
            // With MyClass
            Types5<MyClass> type5_MyClass = new Types5<MyClass>() { Age = new MyClass() };
            Console.WriteLine(type5_MyClass.Age.MyProp.ToString());
            Console.WriteLine("With a class");
            type5_MyClass.DefaultValueForMyGeneric<MyClass>();

            // Impossible: T doit avoir un constructeur sans paramètre
            // Types5<int[]> ddd = new Types5<int[]>(); 
            #endregion

            #region Types5_bis<T>
            #region Array
            Types5_bis_no_New<int[]> type5_bis_no_New_array = new Types5_bis_no_New<int[]>() { PropPubl = new int[] { 1, 2, 3, 4, 5 } };
            Console.WriteLine("With an array");
            type5_bis_no_New_array.DefaultValueForMyGeneric<int[]>();
            Console.WriteLine("First element of the array : " + type5_bis_no_New_array.PropPubl[0].ToString());
            #endregion

            #region Delegate
            Action actForType5_bis_no_New = delegate()
            {
                Console.WriteLine("Vous êtes ici.");
            };

            Types5_bis_no_New<Action> type5_bis_no_New_Delegate = new Types5_bis_no_New<Action>() { PropPubl = actForType5_bis_no_New };
            Console.WriteLine("With a delegate");
            type5_bis_no_New_Delegate.DefaultValueForMyGeneric<Action>();
            type5_bis_no_New_Delegate.PropPubl.Invoke();

            Action<string> actForType5_bis_no_New_Params = delegate (string str)
            {
                Console.WriteLine(str);
            };

            Types5_bis_no_New<Action<string>> type5_bis_no_New_Delegate_Params = new Types5_bis_no_New<Action<string>>() { PropPubl = actForType5_bis_no_New_Params };
            Console.WriteLine("With a delegate with params");
            type5_bis_no_New_Delegate_Params.DefaultValueForMyGeneric<Action>();
            type5_bis_no_New_Delegate_Params.PropPubl.Invoke("You are here");

            #endregion
            #endregion

            #region Interface
            Console.WriteLine("With the interface Interface_for_Type5_no_New.");
            Interface_for_Type5_no_New iClass_Impl = new Class_Impl_Interf_5_no_new ();
            Types5_bis_no_New<Interface_for_Type5_no_New> type5_bis_no_New_Interface = new Types5_bis_no_New<Interface_for_Type5_no_New>() { PropPubl = iClass_Impl };
            
            Console.WriteLine(iClass_Impl.MaClasse.MyProp.ToString());
            type5_bis_no_New_Delegate.DefaultValueForMyGeneric<Interface_for_Type5_no_New>();

            Console.WriteLine("With the interface Interf2_type5_no_new.");
            Interf2_type5_no_new iclass_Interf2_type5_no_new = new Class_Impl_Interf_5_no_new();
            Types5_bis_no_New<Interf2_type5_no_new> type5_bis_no_New_Interface2 = new Types5_bis_no_New<Interf2_type5_no_new>() { PropPubl = iclass_Interf2_type5_no_new };

            Console.WriteLine(iclass_Interf2_type5_no_new.MaClasse.ToString());
            type5_bis_no_New_Interface2.DefaultValueForMyGeneric<Interf2_type5_no_new>();

            #endregion

            #endregion

            #region Change types
            //User_defined.Run();
            #endregion

            #endregion

            #region Day 7
            //AppIEnumerable.Run();
            #endregion

            #region Day 8
            //House.Init();
            /*
            Developer dev = new Developer();
            dev.HelloWorld();
            */
            #endregion
            #endregion

            #region Debug applications and implement security (25%)

            #region Day 11

            #region Working with encryption
            #region Symmetric
            //AESEncryption.EncryptSomeText();
            #endregion

            #region Asymmetric
            //RSAEncryption.EncryptSomeText();
            //KeyContainer.ContainerUsage();
            //SecureStringExample.SecureAsString();
            #endregion
            #endregion

            #endregion

            #region Day 13
            //DebugViewTest.Run();
            #endregion

            #region Day 14
            //DebugExample.HowToUseTheDebugClass();
            //TraceSourceExample.HowToUseTheTraceSourceClass();
            //TraceSourceListenerExample.HowToUseTheTraceListenerClass();
            //EventLogExample.HowToWriteToEventLog();
            //ReadEventLog.HowToReadEventLog();
            //ReadPerformanceCounters.HowToReadPerformanceCounters();
            #endregion
            #endregion

            #region Implement data access (26%)
            #region Day 15
            #region Files directories and drives
            //DirectoryInfoExample.HowToGrantAccess();
            #endregion

            #region Streams
            //CompareFileStreamReader.HowToReadAFile();
            #endregion

            #region Network
            //WebRequestExample.ExampleOfWebRequest();
            #endregion

            #region Async IO streams
            //AsyncExample.HowToUseAsyncProperlyAsync();
            #endregion
            #endregion

            #region Day 18

            #endregion

            #region Day 19
            #region Arrays
            //ArrayExamples.Run();
            #endregion

            #region Using sets
            //HashSetExample.Run();
            #endregion

            #region Queue and stack
            //Queue_Example.Run();
            Stack_Example.Run();
            #endregion

            #endregion

            #endregion

            Console.Read();
        }
    }
}
