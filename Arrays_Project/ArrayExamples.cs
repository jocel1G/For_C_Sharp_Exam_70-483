﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays_Project
{
    public class ArrayExamples
    {
        public static void Run()
        {
            // Single array
            // Ou bien
            //int[] arrayOfInt = new int[10] {0,1,2,3,4,5,6,7,8,9};
            // Ou bien
            int[] arrayOfInt = new int[10];
            arrayOfInt = Enumerable.Range(0, 10).ToArray();


            foreach (int i in arrayOfInt)
            {
                Console.Write(i.ToString());
            }

            Console.WriteLine(Environment.NewLine);

            // Two-dimensional array
            string[,] array2D = new string[3, 2] { { "one", "two" }, { "three", "four" }, { "five", "six" } };
            Console.WriteLine(array2D[0, 1]);
            Console.WriteLine(array2D[2, 0]);

            Console.WriteLine(Environment.NewLine);

            // Jagged array
            int[][] jaggedArray1 =
            {
                new int[] {1,3,5,7,9},
                new int[] {3,4,2}
            };
            Console.WriteLine(jaggedArray1[0][2]);
        }
    }
}
