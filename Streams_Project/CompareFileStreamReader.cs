﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Streams_Project
{
    public static class CompareFileStreamReader
    {
        public static void HowToReadAFile()
        {
            string path = @"C:\temp\test.txt";

            // read a file using a FileStream object
            using(FileStream fileStream = File.OpenRead(path))
            {
                byte[] data = new byte[fileStream.Length];
                for(int index=0; index < fileStream.Length; index++)
                {
                    data[index] = (byte)fileStream.ReadByte();
                }
                Console.WriteLine(Encoding.UTF8.GetString(data));
            }
            Console.WriteLine();
            // read a file using a StreamReader object
            using (StreamReader reader = File.OpenText(path))
            {
                // Ligne ajoutée par rapport à la page Day 15
                while(!reader.EndOfStream)
                    Console.WriteLine(reader.ReadLine());
            }
        }
    }
}
