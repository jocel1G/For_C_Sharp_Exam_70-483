﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Concurrent_Collections
{
    public class BlockingCollectionExample
    {
        public static void Run()
        {
            #region My code

            BlockingCollection<string> col = new BlockingCollection<string>();

            Action actRead = delegate()
            {
                while(true)
                {
                    Console.WriteLine(col.Take());
                }
            };

            Task read = Task.Run(actRead);

            Action actWrite = () =>
                {
                    while(true)
                    {
                        string s = Console.ReadLine();
                        if (string.IsNullOrWhiteSpace(s)) break;
                        col.Add(s);
                    }
                };

            Task write = Task.Run(actWrite);

            write.Wait();
            // Ajout comme suggéré à la page https://msdn.microsoft.com/en-us/library/dd267312(v=vs.110).aspx
            col.Dispose();
            #endregion
            
            #region Code de la page Day 2
            /* 
            BlockingCollection<string> col = new BlockingCollection<string>();
            
            Task read = Task.Run(() =>  
            {  
                while (true)  
                {  
                    Console.WriteLine(col.Take());  
                }  
            }); 
 
            Task write = Task.Run(() =>  
            {  
                while (true)  
                {  
                    string s = Console.ReadLine();  
                    if (string.IsNullOrWhiteSpace(s)) break;  
                    col.Add(s);  
                }  
            });

            write.Wait();
            // Ajout comme suggéré à la page https://msdn.microsoft.com/en-us/library/dd267312(v=vs.110).aspx
            col.Dispose();
            */
            #endregion
        }
    }
}
