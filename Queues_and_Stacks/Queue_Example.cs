﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queues_and_Stacks
{
    public class Queue_Example
    {
        public static void Run()
        {
            Queue<string> numbers = new Queue<string>();
            numbers.Enqueue("one");
            numbers.Enqueue("two");
            numbers.Enqueue("three");
            numbers.Enqueue("four");
            numbers.Enqueue("five");

            // A queue can be enumerated without disturbing its contents.
            foreach(string number in numbers)
            {
                Console.WriteLine(number);
            }

            Console.WriteLine(Environment.NewLine);
            
            // Dequeue
            string strDequeue = numbers.Dequeue();
            Console.WriteLine("After Dequeue: {0}", strDequeue);

            Console.WriteLine(Environment.NewLine);

            // Peek
            Console.WriteLine("Peek: {0}", numbers.Peek());
        }
    }
}
