﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queues_and_Stacks
{
    public class Stack_Example
    {
        public static void Run()
        {
            Stack<string> numbers = new Stack<string>();
            numbers.Push("one");
            numbers.Push("two");
            numbers.Push("three");
            numbers.Push("four");
            numbers.Push("five");

            // A stack can be enumerated without disturbing its content
            foreach (string number in numbers)
            {
                Console.WriteLine(number);
            }

            Console.WriteLine(Environment.NewLine);

            // Pop
            string strPop = numbers.Pop();
            Console.WriteLine("After Pop: {0}", strPop);

            Console.WriteLine(Environment.NewLine);

            // Peek
            Console.WriteLine("Peek: {0}", numbers.Peek());
        }
    }
}
