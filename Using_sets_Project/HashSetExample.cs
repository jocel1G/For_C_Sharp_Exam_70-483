﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Using_sets_Project
{
    public class HashSetExample
    {
        private static HashSet<int> evenSet = new HashSet<int>();
        public static void Run()
        {
            for(int i = 0; i < 10; i++)
            {
                if (i % 2 == 0)
                {
                    evenSet.Add(i);
                }
            }
            DisplaySet(evenSet);
        }

        private static void DisplaySet(HashSet<int> list)
        {
            foreach (int item in list)
            {
                Console.Write(" {0}", item);
            }
        }
    }
}
