﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Working_with_encryption
{
    public class RSAEncryption
    {
        private static string privateKeyXML;
        private static string publicKeyXML;

        public static void EncryptSomeText()
        {
            // Init keys
            GeneratePublicAndPrivateKeys();

            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            byte[] dataToEncrypt = ByteConverter.GetBytes("My ultra secret message!");

            byte[] encryptedData = RSAEncrypt(dataToEncrypt, false);

            byte[] decryptedData = RSADecrypt(encryptedData, false);

            string decryptedString = ByteConverter.GetString(decryptedData);
            Console.WriteLine("Decrypted plaintext: {0}", decryptedString);
        }

        private static void GeneratePublicAndPrivateKeys()
        {
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                publicKeyXML = RSA.ToXmlString(false);
                privateKeyXML = RSA.ToXmlString(true);
            }
        }

        private static byte[] RSAEncrypt(byte[] DataToEncrypt, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                // Create a new instance of RSACryptoServiceProvider
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    // public key used here
                    RSA.FromXmlString(publicKeyXML);
                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
                    return encryptedData;
                }
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private static byte[] RSADecrypt(byte[] DataToDecrypt, bool DoOAEPPadding)
        {
            try
            {
                byte[] decryptedData;
                // Create a new instance of RSACryptoServiceProvider
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    // private key used here
                    RSA.FromXmlString(privateKeyXML);
                    decryptedData = RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
                }

                return decryptedData;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }
    }
}
