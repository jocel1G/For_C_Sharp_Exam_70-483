﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public class Contravariance
    {
        public delegate void ContravarianceDel (StreamWriter tw);
        public static void DoSomething(TextWriter tw) { }

        public static void Run()
        {
            ContravarianceDel del = DoSomething;
        }
    }
}
