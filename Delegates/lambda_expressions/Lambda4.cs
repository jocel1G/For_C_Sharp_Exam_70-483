﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates.lambda_expressions
{
    public class Lambda4
    {
        public static void Run()
        {
            Action myDelegate = getDelegate();
            for (int i = 0; i <= 1; i++ )
                myDelegate();            
        }

        static Action getDelegate()
        {
            int counter = 0;
            /* Ou bien
            return delegate
            {
                Console.WriteLine("counter={0}", counter);
                counter++;
            };
            */
            // Ou bien
            return () =>
            {
                Console.WriteLine("counter={0}", counter);
                counter++;
            };
        }
    }
}
