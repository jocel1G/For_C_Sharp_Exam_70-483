﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public class Delegates_class
    {
        public delegate int Calculate(int x, int y); //declare        

        public static int Add(int x, int y)
        {
            return x + y;
        }

        public static int Multiply(int x, int y)
        {
            return x * y;
        }
        public static void Run()
        {
            Calculate calc = Add; //create and point
            Console.WriteLine(calc(3, 4).ToString()); // invoke

            calc = Multiply; //point
            Console.WriteLine(calc(3, 4).ToString()); // invoke

            // Ajout de ma part
            int invocationCount = calc.GetInvocationList().GetLength(0);
            Console.WriteLine(invocationCount.ToString());

            calc += Add;
            invocationCount = calc.GetInvocationList().GetLength(0);
            Console.WriteLine(invocationCount.ToString());
            Console.WriteLine(calc(3, 4).ToString()); // renvoie 7
        }
    }
}
