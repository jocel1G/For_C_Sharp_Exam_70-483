﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events_and_Callbacks
{
    // Event publisher
    public class Pub3
    {
        // = delegate { } n'est pas utile
        public event EventHandler<MyArgs> onChange; // = delegate { };
        object objectLock = new Object();

        public event EventHandler<MyArgs> OnChange
        {
            add
            {
                lock (objectLock)
                {
                    onChange += value;
                }
            }
            
            remove
            {
                lock (objectLock)
                {
                    onChange -= value;
                }
            }
        }

        public void Run()
        {
            onChange(this, new MyArgs(20));
        }
    }
}
