﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events_and_Callbacks
{
    public class Pub4
    {
        public event EventHandler<EventArgs> OnChange;

        public void Raise()
        {
            var exceptions = new List<Exception>();

            // Un peu différent de ce qui est à la page http://returnsmart.blogspot.fr/2015/07/mcsd-programming-in-c-part-3-70-483.html,
            // mais cela revient au même
            Delegate[] OnChangeGetInvocationList = OnChange.GetInvocationList();

            foreach (Delegate handler in OnChangeGetInvocationList)
            {
                try
                {
                    handler.DynamicInvoke(this, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }
            if (exceptions.Any())
            {
                throw new AggregateException(exceptions);
            }
        }
    }
    public class Events4
    {
        public void CreateAndRaise()
        {
            Pub4 p = new Pub4();

            Action<object, EventArgs> actOnChange1 = (sender, e) =>
            {
                Console.WriteLine("Subscriber 1 called");
            };
            Action<object, EventArgs> actOnChange2 = (sender, e) =>
            {
                throw new Exception();
            };
            Action<object, EventArgs> actOnChange3 = (sender, e) =>
            {
                Console.WriteLine("Subscriber 3 called");
            };
            p.OnChange += (sender, e) => actOnChange1(sender, e);
            p.OnChange += (sender, e) => actOnChange2(sender, e);
            p.OnChange += (sender, e) => actOnChange3(sender, e);

            try
            {
                p.Raise();
            }
            catch (AggregateException ae)
            {
                Console.WriteLine(ae.InnerExceptions.Count.ToString());
            }
        }
    }
}
