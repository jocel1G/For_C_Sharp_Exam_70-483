﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events_and_Callbacks
{
    public class Pub1
    {
        public Action OnChange { get; set; }

        public void Raise()
        {
            if (OnChange != null)
                OnChange();
        }

    }

    public class Events1
    {
        public static void Run()
        {
            Pub1 p = new Pub1();
            Action act1 = () =>
                {
                    Console.WriteLine("Method one raised");
                };
            Action act2 = () =>
            {
                Console.WriteLine("Method two raised");
            };
            // Ou bien
            //p.OnChange = act1 + act2;
            // Ou bien
            p.OnChange += act1;
            p.OnChange += act2;
            p.Raise();
        }
    }
}
