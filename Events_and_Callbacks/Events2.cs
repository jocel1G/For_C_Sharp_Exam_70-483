﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events_and_Callbacks
{
    // Customized argument class
    public class MyArgs: EventArgs
    {
        public int Value { get; set; }

        public MyArgs(int value)
        {
            Value = value;
        }
    }

    // Event publisher
    public class Pub2
    {
        public event EventHandler<MyArgs> OnChange;

        public void Raise()
        {
            // Comme à la page http://returnsmart.blogspot.fr/2015/07/mcsd-programming-in-c-part-3-70-483.html
            ////OnChange(this, new MyArgs(20));

            // Comme à la page https://msdn.microsoft.com/en-us/library/db0etb8x(v=vs.110).aspx
            EventHandler<MyArgs> handler = OnChange;
            if (handler != null)
            {
                handler(this, new MyArgs(20));
            }
        }
    }
    public class Events2
    {
        public static void Run()
        {
            Pub2 p = new Pub2();

            Action<object, MyArgs> actOnChange = (sender, e) =>
                {
                    Console.WriteLine("Event raised:{0}", e.Value);
                };
            p.OnChange += (sender, e) => actOnChange(sender, e);
            p.Raise();
        }
    }
}
