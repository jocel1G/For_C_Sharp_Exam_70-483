﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging_and_tracing
{
    public static class TraceSourceExample
    {
        public static void HowToUseTheTraceSourceClass()
        {
            TraceSource tracesource = new TraceSource("myTraceSource", SourceLevels.All);

            tracesource.TraceInformation("Tracing app");
            tracesource.TraceEvent(TraceEventType.Critical, 0, "Critical trace");
            tracesource.TraceData(TraceEventType.Information, 1, new object[] { "a", "b", "c" });

            tracesource.Flush();
            tracesource.Close();
        }
    }
}
