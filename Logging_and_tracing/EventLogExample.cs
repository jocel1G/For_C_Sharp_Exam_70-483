﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging_and_tracing
{
    public static class EventLogExample
    {
        public const string constMyNewLog = "MyNewLog";

        public static void HowToWriteToEventLog()
        {
            const string constSource = "MySource";

            if (!EventLog.SourceExists(constSource))
            {
                EventLog.CreateEventSource(constSource, constSource);
                Console.WriteLine("CreatedEventSource");
                System.Console.WriteLine("Please restart your application");
                System.Console.ReadKey();
                return;
            }

            EventLog myLog = new EventLog();
            myLog.Source = constSource;
            myLog.WriteEntry("Log event triggered!");
        }
    }
}
