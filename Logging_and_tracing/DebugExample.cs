﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging_and_tracing
{
    public static class DebugExample
    {
        public static void HowToUseTheDebugClass()
        {
            Debug.Write("Here we go!");
            Debug.Indent();
            int i = 1 + 2;
            Debug.Assert(i == 3);
            Debug.WriteLine(i > 0, "i is greater than zero");
        }
    }
}
