﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging_and_tracing
{
    public static class ReadEventLog
    {
        public static void HowToReadEventLog()
        {
            // Reading
            EventLog log = new EventLog(EventLogExample.constMyNewLog);

            int intLogEntriesCount = log.Entries.Count;
            Console.WriteLine("Total entries: {0}", intLogEntriesCount);
            EventLogEntry last = log.Entries[intLogEntriesCount - 1];
            Console.WriteLine("Index: " + last.Index);
            Console.WriteLine("Source: " + last.Source);
            Console.WriteLine("Type: " + last.EntryType);
            Console.WriteLine("Time: " + last.TimeWritten);
            Console.WriteLine("Message: " + last.Message);
        }
    }
}
