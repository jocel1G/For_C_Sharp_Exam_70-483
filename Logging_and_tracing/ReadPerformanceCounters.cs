﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging_and_tracing
{
    public static class ReadPerformanceCounters
    {
        public static void HowToReadPerformanceCounters()
        {
            System.Console.WriteLine("Press escape to exit.");
            using(PerformanceCounter pc = new PerformanceCounter("Memory", "Available bytes"))
            {
                string text = "Available memory: ";
                Console.WriteLine(text);
                do
                {
                    Console.WriteLine(pc.RawValue);
                    Console.SetCursorPosition(text.Length, Console.CursorTop);
                } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
            }
        }
    }
}
