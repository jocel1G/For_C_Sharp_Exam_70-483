﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Using_PLINQ
{
    public class PLINQExample
    {
        public static void Run()
        {
            var numbers = Enumerable.Range(0, 10);
            ParallelQuery<int> parQuer = numbers.AsParallel();

            Predicate<int> predWhere = i =>
                {
                    return i%2 ==0;
                };

            var parallelResult = parQuer.Where(i => predWhere(i)).ToArray();

            foreach (int i in parallelResult)
                Console.WriteLine(i.ToString());
        }
    }
}
