﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Using_PLINQ
{
    public class PLINQExample2
    {
        public static void Run()
        {
            var numbers = Enumerable.Range(0, 10);

            ParallelQuery<int> parQuer = numbers.AsParallel();
            ParallelQuery<int> parQuerOrdered = parQuer.AsOrdered();

            Predicate<int> predWhere = i =>
                {
                    return i%2 ==0;
                };

            var parallelResult = parQuerOrdered.Where(i => predWhere(i)).AsSequential();

            foreach (int i in parallelResult.Take(5))
                Console.WriteLine(i.ToString());
        }
    }
}
