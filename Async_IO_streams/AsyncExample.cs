﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Async_IO_streams
{
    public class AsyncExample
    {
        public static async Task HowToUseAsyncProperlyAsync()
        {
            HttpClient client = new HttpClient();

            Stopwatch clock = new Stopwatch();
            clock.Start();

            Func<string, Task<string>> fctGetStringAsync = delegate(string requestUri)
            {
                return client.GetStringAsync(requestUri);
            };

            //string microsoft = await client.GetStringAsync("http://www.microsoft.com");
            string microsoft = await fctGetStringAsync("http://www.microsoft.com");
            //string msdn = await client.GetStringAsync("http://msdn.microsoft.com"); 
            string msdn = await fctGetStringAsync("http://msdn.microsoft.com"); 
            //string google = await client.GetStringAsync("http://www.google.com");
            string google = await fctGetStringAsync("http://www.google.com");

            clock.Stop();
            Console.WriteLine(@"Took {0:hh\:mm\:ss} seconds", clock.Elapsed);

            clock.Reset();
            clock.Start();

            Task<string> microsoftTask = fctGetStringAsync("http://www.microsoft.com");
            Task<string> msdnTask = fctGetStringAsync("http://msdn.microsoft.com");
            Task<string> googleTask = fctGetStringAsync("http://www.google.com");
            await Task.WhenAll(microsoftTask, msdnTask, googleTask);

            clock.Stop();
            Console.WriteLine(@"Took {0:hh\:mm\:ss} seconds", clock.Elapsed);
        }
    }
}
