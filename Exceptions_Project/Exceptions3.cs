﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions_Project
{
    public class Exceptions3
    {
        public static void Run()
        {
            try
            {

            }
            catch (SumException ex)
            {
                throw new WrongSalaryException("Error while processing your salary", ex);
            }
            finally
            {
                Console.WriteLine("End");
            }
        }

        public static void ProcessArithmeticFunction() { }
    }

    public class WrongSalaryException : Exception
    {
        public WrongSalaryException(string s, Exception e)
        {

        }
    }

    public class SumException : Exception
    {
        
    }
}
