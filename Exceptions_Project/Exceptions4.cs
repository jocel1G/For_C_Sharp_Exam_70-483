﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions_Project
{
    public class Exceptions4
    {
        public static void Run()
        {
            ExceptionDispatchInfo possibleException = null;
            try
            {
                string text = Console.ReadLine();
                int output = int.Parse(text);
            }
            catch (Exception ex)
            {
                possibleException = ExceptionDispatchInfo.Capture(ex) ;
            }

            if (possibleException != null)
            {
                possibleException.Throw();
            }
        }
    }
}
