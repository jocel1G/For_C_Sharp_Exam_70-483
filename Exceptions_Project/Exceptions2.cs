﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions_Project
{
    public class Exceptions2
    {
        public static void Run()
        {
            string s = Console.ReadLine();
            try
            {
                int age = int.Parse(s);
                if (age == 10)
                    Environment.FailFast("Special number was introduced");
            }
            finally
            {
                Console.WriteLine("End");
            }
        }
    }
}
