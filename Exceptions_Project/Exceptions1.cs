﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions_Project
{
    public class Exceptions1
    {
        public static void Run()
        {
            try
            {
                int age = Int32.Parse("Fourteen");
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Error in the format provided.");
            }
            catch (Exception)
            {
                Console.WriteLine("General exception raised.");
                throw;
            }
            
        }
    }
}
