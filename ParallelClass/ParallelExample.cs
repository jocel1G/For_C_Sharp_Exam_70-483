﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelClass
{
    public class ParallelExample
    {
        public static void Run()
        {
            Console.WriteLine(DateTime.Now.TimeOfDay);
            #region First loop
            // Parallel.For Method (Int32, Int32, Action<Int32>)
            // https://msdn.microsoft.com/en-us/library/dd783539(v=vs.110).aspx

            Action<int> body = i =>
            {
                Thread.Sleep(1000);
            };
            Parallel.For(0, 10, body);
            #endregion

            #region Second loop
            // Parallel.ForEach<TSource> Method (IEnumerable<TSource>, Action<TSource>)
            // https://msdn.microsoft.com/en-us/library/dd992001(v=vs.110).aspx

            var numbers = Enumerable.Range(0, 10);
            Action<int> body2 = delegate(int i)
            {
                Thread.Sleep(1000);
            };
            Parallel.ForEach(numbers, body2);
            #endregion

            #region Third loop
            // Parallel.For Method (Int32, Int32, Action<Int32, ParallelLoopState>)
            // https://msdn.microsoft.com/en-us/library/dd783584(v=vs.110).aspx

            Action<int, ParallelLoopState> body3 = (i, loopState) =>
            {
                if (i == 500)
                {
                    Console.WriteLine("Breaking loop");
                    loopState.Break();
                }
                return;
            };

            ParallelLoopResult result = Parallel.For(0, 1000, body3);
            #endregion
            // Ajout de ma part
            Console.WriteLine("result.IsCompleted = " + result.IsCompleted.ToString());

            Console.WriteLine(DateTime.Now.TimeOfDay);
        }
    }
}