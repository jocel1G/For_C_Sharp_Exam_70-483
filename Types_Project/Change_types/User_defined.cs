﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types_Project.Change_types
{
    public class MyMoney
    {
        public decimal Quantity { get; set; }
        public MyMoney(decimal quantity)
        {
            Quantity = quantity;
        }

        public static implicit operator decimal(MyMoney money)
        {
            return money.Quantity;
        }

        public static explicit operator int(MyMoney money)
        {
            return (int)money.Quantity;
        }
    }

    public class User_defined
    {
        public static void Run()
        {
            // Suffixe M : cf. page https://msdn.microsoft.com/en-us/library/364x0z75.aspx
            MyMoney m = new MyMoney(42.42M);
            //This call invokes the implicit "decimal" operator
            decimal amount = m;
            //This call invokes the explicit "int" operator
            int truncatedAmount = (int)m;
            Console.WriteLine(truncatedAmount.ToString());
        }
    }
}
