﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types_Project.Reference_Types
{
    public class MyClass
    {
        public int MyProp { get; set; }

        public MyClass()
        {
            MyProp = 504;
        }
    }
}
