﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Types_Project.Reference_Types;

namespace Types_Project.Generics
{
    public class Class_Impl_Interf_5_no_new: Interface_for_Type5_no_New, Interf2_type5_no_new
    {
        string Interf2_type5_no_new.MaClasse
        {
            get
            {
                return "For the property MaClasse, but string defined in the interface Interf2_type5_no_new.";
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        MyClass Interface_for_Type5_no_New.MaClasse
        {
            get
            {
                return new MyClass() { MyProp = 50 };
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
