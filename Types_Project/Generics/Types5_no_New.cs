﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types_Project.Generics
{
    public class Types5_bis_no_New<T>
        where T:class
    {
        public T PropPubl { get; set; }

        public void DefaultValueForMyGeneric<T>()
        {
            // get default value for my generic
            T defaultValue = default(T);

            // 12/10/2016
            if (defaultValue == null)
                Console.WriteLine("defaultValue = null");
            else
                Console.WriteLine("defaultValue = " + defaultValue);
        }
    }
}
