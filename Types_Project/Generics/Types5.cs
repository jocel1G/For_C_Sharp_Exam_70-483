﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types_Project.Generics
{
    public class Types5<T>
        where T: class, new()
    {
        public Types5()
        {
            Age = new T();
        }

        public T Age { get; set; }

        /*
        // 12/10/2016 : To return the default(T) value
        public T DefaultVal { get; set; }
        */
        public void DefaultValueForMyGeneric<T>()
        {
            // get default value for my generic
            T defaultValue = default(T);

            // 12/10/2016
            if (defaultValue == null)
                Console.WriteLine("defaultValue = null");
        }
    }
}
