﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types_Project.Types_of_types
{
    public class Types2
    {
        [Flags]
        enum Days
        {
            None = 0x0,  
            Sunday = 0x1,  
            Monday = 0x2,  
            Tuesday = 0x4, 
            Wednesday = 0x8, 
            Thursday = 0x10,
            Friday = 0x20,  
            Saturday = 0x40 
        }

        static Days readingDays = Days.Monday | Days.Saturday;
        public static void Run()
        {
            var str = readingDays.ToString();
            Console.WriteLine(str);
            // Comme à la page https://msdn.microsoft.com/en-us/library/sbbt4032.aspx
            Console.WriteLine((int)readingDays);
        }
    }
}
