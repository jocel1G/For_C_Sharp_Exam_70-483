﻿#define Address
#undef Price
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Attributes_Project
{
    [Serializable]
    public class House
    {
        public static void Init()
        {
            if (Attribute.IsDefined(typeof(House), typeof(SerializableAttribute)))
                Console.WriteLine("I'm serializable");  
            else 
                Console.WriteLine("I am not serializable."); 
            
            PrintAddress(); 
            PrintPrice();
            PrintUnderDebug();
        }
        
        [Conditional("Address")] 
        static void PrintAddress()
        { 
            Console.WriteLine("My address goes here."); 
        } 

        [Conditional("Price")] 
        static void PrintPrice()
        { 
            Console.WriteLine("My price goes here."); 
        } 

        [Conditional("DEBUG")]
        static void PrintUnderDebug()
        {
            Console.WriteLine("Under DEBUG.");
        }
    }
}
