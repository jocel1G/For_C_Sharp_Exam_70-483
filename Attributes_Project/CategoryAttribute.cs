﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Attributes_Project
{
    /// <summary>
    /// Category can be used for both classes and methods.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class |
                    AttributeTargets.Method,
                    AllowMultiple=true)]
    public class CategoryAttribute: Attribute
    {
        private string _val;
        public CategoryAttribute(string value)
        {
            _val = value;
        }

        public string Getval()
        {
            return _val;
        }
    }

    public class UnitTestAttribute: CategoryAttribute
    {
        public UnitTestAttribute()
            : base("Unit test")
        {

        }
    }

    public class Developer
    {
        [Category("Human")]
        [CategoryAttribute("Person")]
        [UnitTest]
        public void HelloWorld()
        {
            // Using reflection.
            //System.Attribute[] attrs = System.Attribute.GetCustomAttributes(typeof(Developer));  // Reflection.
            MemberInfo mInfo = typeof(Developer).GetMethod("HelloWorld");
            System.Attribute[] HelloWorldAttribute = Attribute.GetCustomAttributes(mInfo);

            Console.WriteLine(HelloWorldAttribute.Length.ToString());
            Console.WriteLine(HelloWorldAttribute[0].TypeId.ToString());
            foreach(Attribute attr in HelloWorldAttribute)
            {
                if (attr.GetType() == typeof(CategoryAttribute))
                {
                    //Console.WriteLine(attr.ToString());
                    CategoryAttribute cate = (CategoryAttribute)attr;
                    Console.WriteLine(cate.Getval());
                }
                if (attr.GetType() == typeof(UnitTestAttribute))
                {
                    UnitTestAttribute unitest = (UnitTestAttribute)attr;
                    Console.WriteLine(unitest.Getval());
                }
            }

        }
    }
}
