﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Precompiler_Directives
{
    public class DebugViewTest
    {
        // The following constant will appear in the debug window for DebugViewTest.
        const string TabString = "    ";
        // The following DebuggerBrowsableAttribute prevents the property following it
        // from appearing in the debug window for the class.
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static string y = "Test String";

        public static void Run()
        {
            MyHashtable myHashTable = new MyHashtable();
            myHashTable.Add("one", 1);
            myHashTable.Add("two", 2);
            Console.WriteLine(myHashTable.ToString());
            Console.WriteLine("In Main.");
        }

    }
}
