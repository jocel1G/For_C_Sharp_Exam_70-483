﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Precompiler_Directives
{
    [DebuggerDisplay("{value}", Name = "{key}")]
    internal class KeyValuePairs
    {
        private IDictionary dictionary;
        private object key;
        private object value;

        public KeyValuePairs(IDictionary dictionary, object key, object value)
        {
            this.value = value;
            this.key = key;
            this.dictionary = dictionary;
        }
    }

}
