﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr
{
    public static class Threads2
    {
        public static void ThreadMethod(object o)
        {
            for (int i = 0; i < (int)o; i++)
            {
                Console.WriteLine("ThreadProc: {0}", i);
                Thread.Sleep(0);
            }
        }

        /*
        public static void Run()
        {
            // Variant
            ParameterizedThreadStart paramthreadstart = new ParameterizedThreadStart(ThreadMethod);
            Thread t = new Thread(paramthreadstart);
            t.Start(5);
            t.Join();
        }
        */
        // Or
        public static void Run()
        {
            Thread t = new Thread(ThreadMethod);
            t.Start(5);
            t.Join();
        }
        
        
    }
    
}
