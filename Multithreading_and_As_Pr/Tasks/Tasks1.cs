﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public class Tasks1
    {
        public static void Run()
        {
            Action actionForTask = () => {
                for (int x=0; x <= 100; x++)
                    Console.WriteLine('*');
            };
            // Ou bien
            //Task t = Task.Run(actionForTask);
            // Ou bien
            Task t = Task.Factory.StartNew(actionForTask);
            // Comme à la page https://msdn.microsoft.com/en-us/library/system.threading.tasks.task(v=vs.110).aspx
            t.Wait();
        }
    }
}
