﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public static class Tasks8
    {
        public static void Run()
        {
            #region My code
            
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken token = cancellationTokenSource.Token;

            Action<CancellationToken> actTaskRun = delegate(CancellationToken ptoken)
            {
                while (!ptoken.IsCancellationRequested)
                {
                    Console.Write("*");
                    Thread.Sleep(500);
                }      
                    

                ptoken.ThrowIfCancellationRequested();
            };

            Task task = Task.Run(() => actTaskRun(token), token);

            try
            {
                Console.WriteLine("Press enter to stop the task");
                Console.ReadLine();

                cancellationTokenSource.Cancel();
                task.Wait();
            }
            catch (AggregateException ae)
            {
                Console.WriteLine(ae.InnerExceptions[0].Message);
            }
            
            #endregion

            #region Code de la page
            /*
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken token = cancellationTokenSource.Token;
 
            Task task = Task.Run(() =>  
            {  
                while (!token.IsCancellationRequested)  
                {  
                  Console.Write("*");  
                  Thread.Sleep(500);  
                }
 
                token.ThrowIfCancellationRequested();  
            }, token); 
 
            try  
            {  
              Console.WriteLine("Press enter to stop the task");  
              Console.ReadLine(); 
 
              cancellationTokenSource.Cancel();  
              task.Wait();  
            }  
            catch (AggregateException e)  
            {  
                Console.WriteLine(e.InnerExceptions[0].Message);  
            } 
            */
            #endregion
        }
    }
}
