﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public static class Tasks4
    {
        public static void Run()
        {
            Func<int> fctRet42 = delegate()
            {
                return 42;
            };

            Task<int> t = Task<int>.Run(fctRet42);

            // Either
            /*
            Action<Task<int>> actCanceled = ((i) =>
            {
                Console.WriteLine("Canceled");
            });
            */
            // Or
            Action<Task<int>> actCanceled = delegate(Task<int> i)
            {
                Console.WriteLine("Canceled");
            };

            Action<Task<int>> actFaulted = ((i) =>
            {
                Console.WriteLine("Faulted");
            });

            Action<Task<int>> actCompleted = ((i) =>
            {
                Console.WriteLine("Completed");
            });

            t.ContinueWith(actCanceled, TaskContinuationOptions.OnlyOnCanceled);

            t.ContinueWith(actFaulted, TaskContinuationOptions.OnlyOnFaulted);

            var completedTask = t.ContinueWith(actCompleted, TaskContinuationOptions.OnlyOnRanToCompletion);

            completedTask.Wait();

            Console.WriteLine(t.Result);
        }
    }
}
