﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public static class Tasks3
    {
        public static void Run()
        {
            Func<int> funcForTask = () =>
            {
                return 42;
            };
            Func<Task<int>, int> funcContinueWith = t =>
            {
                return t.Result * 2;
            };

            Task<int> tmain = Task<int>.Run(funcForTask).ContinueWith(funcContinueWith);

            Console.WriteLine(tmain.Result);

            tmain.Wait();
        }
    }
}
