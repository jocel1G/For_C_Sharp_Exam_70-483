﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public static class Tasks5
    {
        public static void Run()
        {
            #region My version

            var results = new Int32[3];
            Func<Int32[]> fctResults = delegate()
            {
                Action<int> actRes = (i =>
                                results[i] = i);

                new Task(() => actRes(0),
                    TaskCreationOptions.AttachedToParent).Start();
                new Task(() => actRes(1),
                    TaskCreationOptions.AttachedToParent).Start();
                new Task(() => actRes(2),
                    TaskCreationOptions.AttachedToParent).Start();
                return results;
            };
            Task<Int32[]> parent = Task.Run(fctResults);

            Action<Task<Int32[]>> actTa = parentTask =>
                {
                    foreach (int i in parentTask.Result)
                        Console.WriteLine(i);
                };

            var finalTask = parent.ContinueWith(actTa);
            finalTask.Wait();

            #endregion

            #region From Page
            // À partir de la page http://returnsmart.blogspot.fr/2015/06/mcsd-programming-in-c-part-1-70-483.html
            /*
            Task<Int32[]> parent = Task.Run(() =>  
                { 
                    var results = new Int32[3]; 
                    new Task(() => results[0] = 0, 
                        TaskCreationOptions.AttachedToParent).Start(); 
                    new Task(() => results[1] = 1, 
                        TaskCreationOptions.AttachedToParent).Start();
                    new Task(() => results[2] = 2,
                        TaskCreationOptions.AttachedToParent).Start();           
 
                    return results; 
                });
 
            var finalTask = parent.ContinueWith(
                parentTask => 
                { 
                    foreach (int i in parentTask.Result) 
                        Console.WriteLine(i);
                }); 
            finalTask.Wait();
            */
            #endregion
        }
    }
}
