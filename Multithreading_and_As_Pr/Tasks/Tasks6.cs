﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public static class Tasks6
    {
        public static void Run()
        {
            #region My version
            /*
            Func<Int32[]> fctResults = delegate()
            {
                var results = new Int32[3];

                Action<int> actRes = (i =>
                                results[i] = i);

                TaskFactory tf = new TaskFactory(TaskCreationOptions.AttachedToParent,
                    TaskContinuationOptions.ExecuteSynchronously);

                tf.StartNew(() => actRes(0));
                tf.StartNew(() => actRes(1));
                tf.StartNew(() => actRes(2));
                
                return results;
            };

            Task<Int32[]> parent = Task.Run(fctResults);

            Action<Task<Int32[]>> actTa = parentTask =>
            {
                foreach (int i in parentTask.Result)
                    Console.WriteLine(i);
            };

            var finalTask = parent.ContinueWith(actTa);
            finalTask.Wait();
            */
            #endregion

            #region From Page
            
            Task<Int32[]> parent = Task.Run(() => 
            { 
                var results = new Int32[3]; 

                TaskFactory tf = new TaskFactory(TaskCreationOptions.AttachedToParent,                  
                    TaskContinuationOptions.ExecuteSynchronously); 
                
                tf.StartNew(() => results[0] = 0); 
                tf.StartNew(() => results[1] = 1);
                tf.StartNew(() => results[2] = 2); 

                return results; 
            }); 

            var finalTask = parent.ContinueWith( 

            parentTask => 
            {
                 foreach (int i in parentTask.Result)
                     Console.WriteLine(i); 
            }); 

            finalTask.Wait(); 
            #endregion

        }
    }
}
