﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public static class Tasks7
    {
        public static void Run()
        {
            Func<int, int, int> fctTaskRun = (delay, intRet) =>
                {
                    Thread.Sleep(delay);
                    return intRet;
                };

            Task<int>[] tasks = new Task<int>[3];
            tasks[0] = Task.Run(() => fctTaskRun(2000, 1));
            tasks[1] = Task.Run(() => fctTaskRun(1000, 2));
            tasks[2] = Task.Run(() => fctTaskRun(3000, 3));

            while(tasks.Length > 0)
            {
                int i = Task.WaitAny(tasks);
                Task<int> completedTask = tasks[i];

                Console.WriteLine(completedTask.Result.ToString());

                var temp = tasks.ToList();
                temp.RemoveAt(i);
                tasks = temp.ToArray();
            }
        }
    }
}
