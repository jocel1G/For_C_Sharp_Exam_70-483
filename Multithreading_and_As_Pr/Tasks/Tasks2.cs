﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public static class Tasks2
    {
        public static void Run()
        {
            Func<int> funcForTask = () =>
            {
                return 42;
            };

            // Ou bien
            //Task<int> t = Task.Run(funcForTask);
            // Ou bien, comme à la page https://msdn.microsoft.com/en-us/library/dd321424(v=vs.110).aspx (Task<TResult> Class) :
            Task<int> t = Task<int>.Run(funcForTask);

            Console.WriteLine(t.Result);
        }
    }
}
