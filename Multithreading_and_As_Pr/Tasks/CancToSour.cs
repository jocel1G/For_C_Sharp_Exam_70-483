﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr.Tasks
{
    public static class CancToSour
    {
        public static void Run()
        {
            #region My code
            // Define the cancellation token
            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;

            Random rnd = new Random();
            Object lockObj = new Object();

            List<Task<int[]>> tasks = new List<Task<int[]>>();
            TaskFactory factory = new TaskFactory(token);

            Func<object, int[]> fctStartNew = delegate(object p_iteration)
            {
                int value;
                int[] values = new int[10];
                for (int ctr=1; ctr <= 10; ctr++)
                {
                    lock(lockObj)
                    {
                        value = rnd.Next(0, 101);
                    }
                    if (value == 0)
                    {
                        source.Cancel();
                        Console.WriteLine("Cancelling at task {0}", (int)p_iteration);
                        break;
                    }
                    values[ctr-1] = value;
                }

                return values;
            };

            for (int taskCtr = 0; taskCtr <= 10; taskCtr++)
            {
                int iteration = taskCtr + 1;
                tasks.Add(factory.StartNew(fctStartNew, iteration, token));
            }

            try
            {
                Func<Task<int[]>[], double> continuationFunction = (results =>
                    {
                        Console.WriteLine("calculating overall mean...");
                        long sum = 0;
                        int n = 0;
                        foreach(var t in results)
                        {
                            foreach (var r in t.Result)
                            {
                                sum += r;
                                n++;
                            }
                        }
                        return sum / (double)n;

                    }
                );

                Task<double> fTask = factory.ContinueWhenAll(tasks.ToArray(), continuationFunction, token);
                // 19/05/2016 : Ajout de ma part. Mais non : il faut accéder à fTask.Result
                // pour avoir l'exception TaskCanceledException
                // if (!token.IsCancellationRequested) //non
                    Console.WriteLine("The mean is {0}", fTask.Result);
                /* non
                else
                    rethrow new Exception();
                non */
            }
            catch (AggregateException ae)
            {
                foreach(Exception e in ae.InnerExceptions)
                {
                    if (e is TaskCanceledException)
                        Console.WriteLine("Unable to compute mean: {0}",
                                    ((TaskCanceledException)e).Message);
                    else
                        Console.WriteLine("Exception: " + e.GetType().Name);
                }
            }
            finally
            {
                source.Dispose();
            }
            #endregion

            #region Code de la page
            //// Define the cancellation token.
            //CancellationTokenSource source = new CancellationTokenSource();
            //CancellationToken token = source.Token;

            //Random rnd = new Random();
            //Object lockObj = new Object();

            //List<Task<int[]>> tasks = new List<Task<int[]>>();
            //TaskFactory factory = new TaskFactory(token);
            //for (int taskCtr = 0; taskCtr <= 10; taskCtr++)
            //{
            //    int iteration = taskCtr + 1;
            //    tasks.Add(factory.StartNew(() =>
            //    {
            //        int value;
            //        int[] values = new int[10];
            //        for (int ctr = 1; ctr <= 10; ctr++)
            //        {
            //            lock (lockObj)
            //            {
            //                value = rnd.Next(0, 101);
            //            }
            //            if (value == 0)
            //            {
            //                source.Cancel();
            //                Console.WriteLine("Cancelling at task {0}", iteration);
            //                break;
            //            }
            //            values[ctr - 1] = value;
            //        }
            //        return values;
            //    }, token));

            //}
            //try
            //{
            //    Task<double> fTask = factory.ContinueWhenAll(tasks.ToArray(),
            //                                                 (results) =>
            //                                                 {
            //                                                     Console.WriteLine("Calculating overall mean...");
            //                                                     long sum = 0;
            //                                                     int n = 0;
            //                                                     foreach (var t in results)
            //                                                     {
            //                                                         foreach (var r in t.Result)
            //                                                         {
            //                                                             sum += r;
            //                                                             n++;
            //                                                         }
            //                                                     }
            //                                                     return sum / (double)n;
            //                                                 }, token);
            //    Console.WriteLine("The mean is {0}.", fTask.Result);
            //}
            //catch (AggregateException ae)
            //{
            //    foreach (Exception e in ae.InnerExceptions)
            //    {
            //        if (e is TaskCanceledException)
            //            Console.WriteLine("Unable to compute mean: {0}",
            //                              ((TaskCanceledException)e).Message);
            //        else
            //            Console.WriteLine("Exception: " + e.GetType().Name);
            //    }
            //}
            //finally
            //{
            //    source.Dispose();
            //}

            #endregion

        }
    }
}
