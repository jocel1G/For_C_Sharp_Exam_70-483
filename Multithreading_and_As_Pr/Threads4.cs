﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr
{
    public static class Threads4
    {
        public static Func<int> valueFactory = () =>
            {
                return Thread.CurrentThread.ManagedThreadId;
            };

        public static ThreadLocal<int> _field =
            new ThreadLocal<int>(valueFactory);

        public static void Run()
        {
            /*
            ThreadStart threadstartA = delegate()
            {
                for (int x = 0; x < _field.Value; x++)
                {
                    Console.WriteLine("Thread A: {0}", x);                    
                }
            };

            ThreadStart threadstartB = delegate()
            {
                for (int x = 0; x < _field.Value; x++)
                {
                    Console.WriteLine("Thread B: {0}", x);                    
                }
            };

            new Thread(threadstartA).Start();
            new Thread(threadstartB).Start();
            */
            // Or

            ////Either
            /*
            ParameterizedThreadStart paramthreadstart = delegate(object strAOrB)
            {
                for (int x = 0; x < _field.Value; x++)
                {
                    Console.WriteLine("Thread " + (string)strAOrB + ": {0}", x);
                }
            };
            */
            ////Or
            ParameterizedThreadStart paramthreadstart = (strAOrB) =>
                {
                    for (int x = 0; x < _field.Value; x++)
                    {
                        Console.WriteLine("Thread " + (string)strAOrB + ": {0}", x);
                    }
                };

            Thread threadA = new Thread(paramthreadstart);
            threadA.Start("A");
            Thread threadB = new Thread(paramthreadstart);
            threadB.Start("B");
        }
    }
}
