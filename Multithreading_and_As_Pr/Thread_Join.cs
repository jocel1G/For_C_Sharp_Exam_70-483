﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr
{
    // À partir de https://msdn.microsoft.com/en-us/library/95hbf2ta.aspx
    public class Thread_Join
    {
        static Thread thread1, thread2;
        private const string constThread1Name = "Thread1";
        private const string constThread2Name = "Thread2";

        public static void Run()
        {
            thread1 = new Thread(ThreadProc);
            thread1.Name = constThread1Name;
            thread1.Start();

            thread2 = new Thread(ThreadProc);
            thread2.Name = constThread2Name;
            thread2.Start();
        }

        private static void ThreadProc()
        {
            string strThreadCurrentName = Thread.CurrentThread.Name;

            Console.WriteLine("\nCurrent thread: {0}", strThreadCurrentName);
            // If the current thread is Thread1, and the method Thread.Start method has already been invoked on the thread Thread2. 
            if (strThreadCurrentName == constThread1Name &&
                thread2.ThreadState != ThreadState.Unstarted)
            {
                // The calling thread is Thread1. It is blocked, waiting for the thread Thread2 to complete.
                thread2.Join();
            }

            Thread.Sleep(4000);
            Console.WriteLine("\nCurrent thread: {0}", strThreadCurrentName);
            Console.WriteLine("Thread1: {0}", thread1.ThreadState);
            Console.WriteLine("Thread2: {0}", thread2.ThreadState);
        }
    }
}
