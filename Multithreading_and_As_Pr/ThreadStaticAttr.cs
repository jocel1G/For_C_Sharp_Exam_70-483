﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr
{
    public class ThreadStaticAttr
    {
        [ThreadStatic]
        static double previous = 0.0;
        [ThreadStatic]
        static double sum = 0.0;
        [ThreadStatic]
        static int calls = 0;
        [ThreadStatic]
        static bool abnormal;
        static int totalNumbers = 0;
        static CountdownEvent countdown;
        private static object lockObj;
        Random rand;

        public ThreadStaticAttr()
        {
            rand = new Random();
            // À revoir : membres statiques dans un constructeur non statique.
            // 09/05/2016 : lignes déplacées dans le constructeur statique,
            // comme nous le lisons à la page https://msdn.microsoft.com/en-us/library/k6sa6h87.aspx
            /*
            lockObj = new object();
            countdown = new CountdownEvent(1);
            */ 
        }

        // See page https://msdn.microsoft.com/en-us/library/k9x6w0hc.aspx
        static ThreadStaticAttr()
	    {
            lockObj = new object();
            countdown = new CountdownEvent(1);
	    }

        public static void Run()
        {
            ThreadStaticAttr threadstaticattr = new ThreadStaticAttr();
            Thread.CurrentThread.Name = "Main";
            threadstaticattr.Execute();
            countdown.Wait();
            Console.WriteLine("{0:N0} random numbers were generated.", totalNumbers);
        }

        private void Execute()
        {
            for (int threads = 1; threads <= 10; threads++)
            {
                ThreadStart threadstart = new ThreadStart(this.GetRandomNumbers);
                Thread newThread = new Thread(threadstart);
                countdown.AddCount();
                newThread.Name = threads.ToString();
                newThread.Start();
            }
            this.GetRandomNumbers();
        }

        private void GetRandomNumbers()
        {
            double result = 0.0;

            for (int ctr = 0; ctr < 2000000; ctr++)
            {
                // Cf. page https://msdn.microsoft.com/en-us/library/c5kehkcz.aspx pour l'explication.
                lock (lockObj)
                {
                    result = rand.NextDouble();
                    calls++;
                    Interlocked.Increment(ref totalNumbers);
                    // We should never get the same random number twice.
                    if (result == previous)
                    {
                        abnormal = true;
                        break;
                    }
                    else
                    {
                        previous = result;
                        sum += result;
                    }
                }
            }

            string strThreadCurrentName = Thread.CurrentThread.Name;

            // Get last result
            if (abnormal)
                Console.WriteLine("Result is {0} in {1}", previous, strThreadCurrentName);

            Console.WriteLine("Thread {0} finished random number generation.", strThreadCurrentName);
            Console.WriteLine("Sum = {0:N4}, Mean = {1:N4}, n = {2:N0}", sum, sum / calls, calls);
            countdown.Signal();
        }
    }
}
