﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading_and_As_Pr
{
    public static class Threads3
    {
        public static void Run()
        {
            bool stopped = false;
            // Variant (lambda expression at page http://returnsmart.blogspot.fr/2015/06/mcsd-programming-in-c-part-1-70-483.html)
            ThreadStart threadstart_del = delegate()
            {
                while (!stopped)
                {
                    Console.WriteLine("Running...");
                    Thread.Sleep(1000);
                }
            };

            Thread t = new Thread(threadstart_del);
            t.Start();

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

            stopped = true;

            t.Join();
        }
    }
}
