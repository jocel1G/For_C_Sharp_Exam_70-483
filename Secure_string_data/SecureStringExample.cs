﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Secure_string_data
{
    public class SecureStringExample
    {
        public static void SecureAsString()
        {
            using (SecureString ss = new SecureString()) // IDisposable implemented
            {
                Console.WriteLine("Please enter a password:");
                while(true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);
                    if (cki.Key == ConsoleKey.Enter)
                        break;

                    ss.AppendChar(cki.KeyChar);
                    Console.Write("*");
                }
                Console.WriteLine("\nPassword encrypted and stored successfully!");
                ConvertToUnsecureString(ss);
                ss.MakeReadOnly();
            }
        }

        private static void ConvertToUnsecureString(SecureString securePassword)
        {
            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                Console.WriteLine(Marshal.PtrToStringUni(unmanagedString));
            }
            catch (Exception)
            {
               Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }
    }
}
